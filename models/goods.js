const mongoose = require('mongoose');
const { Schema } = mongoose;

const goodsSchema = new Schema({
    displayName: {
        type: String,
        required: true,
        unique: true,
        minlength: 3
    },
    desc: {
        type: String,
        required: true,
        minlength: 20
    },
    price: {
        type: Number,
        default: 0,
        min: 0
    }, //TODO: add rel to currency

})

const GoodsModel = mongoose.model('goods', goodsSchema);
module.exports = GoodsModel;