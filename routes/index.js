const express = require('express');
const router = express.Router();
const { logTime } = require('../middlewares/timeLoggerMiddleware')
const multer  = require('multer')
const upload = multer({ dest: 'uploads/' })

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/acticles', function(req, res, next) {
  res.send('mock articles');
});

// *** динамические роуты. ***
router.get('/acticles/:article', function(req, res, next) {
  res.send(req.params.article);
});

router.get('/acticles/:article/:id', function(req, res, next) {
  const { article, id } = req.params;
  res.send(article + id);
});

// *** end ***

//  *** query params ***

router.get('/querytest/:id?',logTime , function(req, res, next) {
  console.log(req.time) // выводит время.
  const test =  Object.keys(req.query).reduce(
    (acc, currentVal)=> acc + ' ' + currentVal
  ); // автоматически выводит ключи
  
  res.send(test);
});

// *** end  ***

// ** forms ** //

router.post('/form', upload.single('imgs'),function(req, res, next) {
  res.send('xuevo');
});

// *** end *** //

module.exports = router;
