const express = require('express');
const router = express.Router();
const Goods = require('../models/goods');

router.get('/good/staticAdd', async (req, res)=>{
    try {
        
        const good = await Goods.create({
            displayName: 'good'+Date.now(),
            desc: 'My awesome desc for my trivial good, i`m not making out this from my head:_)'
        })

        res.status(200).json(good)

    } catch (error) {
        res.send('XUEVO DB')
    }
});

router.get('/goods/all', async (req, res)=>{
    try {
        console.log('hi there')
        const good = await Goods.find({})
 
        res.status(200).json(good)

    } catch (error) {
        console.log('errr', error)
        res.send('XUEVO DB')
    }
});

router.get('/goods/:id', async (req, res)=>{
    try {
        console.log('hi there')
        const good = await Goods.findById(req.params.id);

        res.render('good', good)

    } catch (error) {
        console.log('errr', error)
        res.send('XUEVO DB')
    }
});

module.exports = router;

// router.get('/addGood', async (req, res)=>{
//     try {
        
        

//     } catch (error) {
//         res.send('XUEVO DB')
//     }
// });
